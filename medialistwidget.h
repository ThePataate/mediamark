#pragma once

#include <QWidget>
#include <QTableView>
#include <QHBoxLayout>
#include <QHeaderView>

#include <mediaquerymodel.h>

/**
 * @brief The MediaListWidget class
 *
 * Used to display a list a media (specified via the type)
 */
class MediaListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MediaListWidget(MediaType type, QWidget *parent = nullptr);
protected:
    /**
     * @brief model
     *
     * QSqlQueryModel child class used as template model for the tableview
     */
    MediaQueryModel *model;

    /**
     * @brief tableView
     *
     * TableView used to display data
     */
    QTableView *tableView;

    /**
     * @brief type
     *
     * The type displayed by this list
     */
    MediaType type;
public slots:
    void GoToUpdatePage(const QModelIndex& index);

    void Refresh();
signals:
    /**
     * Signal emitted when update is clicked
     *
     * @brief UpdateButtonClicked
     * @param id
     */
    void UpdateButtonClicked(int id);

    /**
     * Signal emitted when add to list is clicked
     *
     * @brief AddToListButtonClicked
     * @param id
     */
    void AddToListButtonClicked(int id);
};
