#include "tv_show.h"

Tv_show::Tv_show(int id_)
{
    id = id_;

    //We initialize properties
    name = "";
    synopsis = "";
    release_date = QDate::currentDate();
    director = "";
    end_date = QDate::currentDate();
    season_number = 0;

    if(id)
    {
        QSqlQuery query = MMDatabase::Query("SELECT name, synopsis, release_date, director, end_date, season_number FROM tv_show INNER JOIN media ON tv_show.media_id = media.id WHERE id = ?", {QString::number(id)});

        if(query.next())
        {
            name = query.value(0).toString();
            synopsis = query.value(1).toString();
            release_date = query.value(2).toDate();
            director = query.value(3).toString();
            end_date = query.value(4).toDate();
            season_number = query.value(5).toInt();
        }
    }
}

void Tv_show::Save()
{
    if(id)
    {
        MMDatabase::Query(
            "UPDATE media SET name = ?, synopsis = ?, release_date = ?, update_datetime = ? WHERE id = ?",
            {
              name,
              synopsis,
              release_date.toString(Qt::ISODate),
              QDateTime::currentDateTime().toString(Qt::ISODate),
              QString::number(id)
            }
        );

        MMDatabase::Query(
            "UPDATE tv_show SET director = ?, end_date = ?, season_number = ? WHERE media_id = ?",
            {
                director,
                end_date.toString(Qt::ISODate),
                QString::number(season_number),
                QString::number(id)
            }
        );
    }else
    {
        MMDatabase::Query(
            "INSERT INTO media (name, synopsis, release_date, creation_datetime) VALUES (?,?,?,?)",
            {
                name,
                synopsis,
                release_date.toString(Qt::ISODate),
                QDateTime::currentDateTime().toString(Qt::ISODate),
            }
        );
        id = MMDatabase::LastId();
        MMDatabase::Query(
            "INSERT INTO tv_show (media_id, director, end_date, season_number) VALUES (?,?,?,?)",
            {
                QString::number(id),
                director,
                end_date.toString(Qt::ISODate),
                QString::number(season_number)
            }
        );
    }
}

void Tv_show::GetForm(QWidget *w)
{
    QVBoxLayout *vLayout = new QVBoxLayout;

    QLabel *titleLabel = new QLabel;
    titleLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    QFont font = QFont();
    font.setPointSize(25);
    font.setBold(true);
    titleLabel->setFont(font);
    titleLabel->setText("Tv Show");

    vLayout->addWidget(titleLabel, 0, Qt::AlignCenter);

    QFormLayout *layout = new QFormLayout;
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout->setFormAlignment(Qt::AlignHCenter|Qt::AlignTop);
    layout->setVerticalSpacing(50);
    layout->setContentsMargins(10, 10, 10, 10);


    QLineEdit *nameEdit = new QLineEdit(name);
    QTextEdit *synopsisEdit = new QTextEdit(synopsis);
    QLineEdit *directorEdit = new QLineEdit(director);
    QSpinBox *seasonEdit = new QSpinBox;
    seasonEdit->setValue(season_number);
    seasonEdit->setMaximum(99);
    QDateEdit *dateEdit = new QDateEdit(release_date);
    QDateEdit *endDateEdit = new QDateEdit(end_date);

    layout->addRow("Name", nameEdit);
    layout->addRow("Synopsis", synopsisEdit);
    layout->addRow("Director", directorEdit);
    layout->addRow("Season Number", seasonEdit);
    layout->addRow("Release Date", dateEdit);
    layout->addRow("End Date", endDateEdit);

    vLayout->addLayout(layout);

    QPushButton *saveButton = new QPushButton;
    saveButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    saveButton->setText("Save");

    vLayout->addWidget(saveButton, 0, Qt::AlignHCenter);

    w->setLayout(vLayout);

    QObject::connect(saveButton, SIGNAL(clicked()), w, SLOT(SaveForm()));
}

void Tv_show::UpdateFromForm(QWidget *w)
{
    name = w->children().at(3)->property("text").toString();
    synopsis = w->children().at(5)->property("plainText").toString();
    director = w->children().at(7)->property("text").toString();
    season_number = w->children().at(9)->property("value").toInt();
    release_date = w->children().at(11)->property("date").toDate();
    end_date = w->children().at(13)->property("date").toDate();
}
