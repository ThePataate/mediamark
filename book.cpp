#include "book.h"

Book::Book(int id_)
{
    id = id_;

    //We initialize properties
    name = "";
    synopsis = "";
    release_date = QDate::currentDate();
    author = "";
    isbn = "";
    number_pages = 0;

    if(id)
    {
        QSqlQuery query = MMDatabase::Query("SELECT name, synopsis, release_date, author, ISBN, number_pages FROM book INNER JOIN media ON book.media_id = media.id WHERE id = ?", {QString::number(id)});

        if(query.next())
        {
            name = query.value(0).toString();
            synopsis = query.value(1).toString();
            release_date = query.value(2).toDate();
            author = query.value(3).toString();
            isbn = query.value(4).toString();
            number_pages = query.value(5).toInt();
        }
    }
}

void Book::Save()
{
    if(id)
    {
        MMDatabase::Query(
            "UPDATE media SET name = ?, synopsis = ?, release_date = ?, update_datetime = ? WHERE id = ?",
            {
              name,
              synopsis,
              release_date.toString(Qt::ISODate),
              QDateTime::currentDateTime().toString(Qt::ISODate),
              QString::number(id)
            }
        );

        MMDatabase::Query(
            "UPDATE book SET author = ?, ISBN = ?, number_pages = ? WHERE media_id = ?",
            {
                author,
                isbn,
                QString::number(number_pages),
                QString::number(id)
            }
        );
    }else
    {
        MMDatabase::Query(
            "INSERT INTO media (name, synopsis, release_date, creation_datetime) VALUES (?,?,?,?)",
            {
                name,
                synopsis,
                release_date.toString(Qt::ISODate),
                QDateTime::currentDateTime().toString(Qt::ISODate),
            }
        );
        id = MMDatabase::LastId();
        MMDatabase::Query(
            "INSERT INTO book (media_id, author, ISBN, number_pages) VALUES (?,?,?,?)",
            {
                QString::number(id),
                author,
                isbn,
                QString::number(number_pages)
            }
        );
    }
}

void Book::GetForm(QWidget *w)
{
    QVBoxLayout *vLayout = new QVBoxLayout;

    QLabel *titleLabel = new QLabel;
    titleLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    QFont font = QFont();
    font.setPointSize(25);
    font.setBold(true);
    titleLabel->setFont(font);
    titleLabel->setText("Book");

    vLayout->addWidget(titleLabel, 0, Qt::AlignCenter);

    QFormLayout *layout = new QFormLayout;
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout->setFormAlignment(Qt::AlignHCenter|Qt::AlignTop);
    layout->setVerticalSpacing(50);
    layout->setContentsMargins(10, 10, 10, 10);


    QLineEdit *nameEdit = new QLineEdit(name);
    QTextEdit *synopsisEdit = new QTextEdit(synopsis);
    QLineEdit *isbnEdit = new QLineEdit(isbn);
    QSpinBox *pagesEdit = new QSpinBox;
    pagesEdit->setValue(number_pages);
    pagesEdit->setMaximum(3000);
    QLineEdit *authorEdit = new QLineEdit(author);
    QDateEdit *dateEdit = new QDateEdit(release_date);

    layout->addRow("Name", nameEdit);
    layout->addRow("Synopsis", synopsisEdit);
    layout->addRow("ISBN", isbnEdit);
    layout->addRow("Number of pages", pagesEdit);
    layout->addRow("Author", authorEdit);
    layout->addRow("Release Date", dateEdit);

    vLayout->addLayout(layout);

    QPushButton *saveButton = new QPushButton;
    saveButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    saveButton->setText("Save");

    vLayout->addWidget(saveButton, 0, Qt::AlignHCenter);

    w->setLayout(vLayout);

    QObject::connect(saveButton, SIGNAL(clicked()), w, SLOT(SaveForm()));
}

void Book::UpdateFromForm(QWidget *w)
{
    name = w->children().at(3)->property("text").toString();
    synopsis = w->children().at(5)->property("plainText").toString();
    isbn = w->children().at(7)->property("text").toString();
    number_pages = w->children().at(9)->property("value").toInt();
    author = w->children().at(11)->property("text").toString();
    release_date = w->children().at(13)->property("date").toDate();
}
