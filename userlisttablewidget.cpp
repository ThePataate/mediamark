#include "userlisttablewidget.h"

/**
 * Constructor
 *
 * @brief UserListTableWidget::UserListTableWidget
 * @param parent
 */
UserListTableWidget::UserListTableWidget(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *hLayout = new QHBoxLayout;

    model = new UserListQueryModel;

    tableView = new QTableView(this);
    tableView->setModel(model);
    tableView->setCornerButtonEnabled(false);
    tableView->setSortingEnabled(false);
    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    //TODO Need to implement a proper sorting, does nothing for now
    tableView->sortByColumn(1, Qt::AscendingOrder);

    //Id column (used to keep track of row's corresponding media id)
    tableView->hideColumn(0);

    hLayout->addWidget(tableView);
    setLayout(hLayout);

    QObject::connect(tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(GoToUpdatePage(QModelIndex)));
}


/**
 * Checks the double click of "update" cell
 *
 * @brief GoToUpdatePage
 * @param index
 */
void UserListTableWidget::GoToUpdatePage(const QModelIndex &index)
{
    if(index.column() == 6)
    {
        emit UpdateButtonClicked(model->index(index.row(), 0).data().toInt());
    }
}

/**
 * Refreshs this tableview data
 *
 * @brief UserListTableWidget::Refresh
 */
void UserListTableWidget::Refresh()
{
    ((UserListQueryModel*)tableView->model())->Refresh();
}
