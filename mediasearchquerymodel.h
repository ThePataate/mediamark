#pragma once

#include <QSqlQueryModel>
#include <QString>

#include <mmdatabase.h>

class MediaSearchQueryModel : public QSqlQueryModel
{
public:
    explicit MediaSearchQueryModel(QString filter, QObject *parent = nullptr);

    void SetCustomQuery(QString filter);

    // QAbstractItemModel interface
public:
    QVariant data(const QModelIndex &index, int role) const override;
};
