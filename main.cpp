#include <QApplication>

#include <mmdatabase.h>
#include <mmmainwindow.h>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    //Connection to the sqlite database and setting it up (if needed)
    MMDatabase::ConnectToDatabase();
    MMDatabase::SetupDatabase();

    //Opens the custom main window
    MMMainWindow mainWindow;
    mainWindow.show();

    return app.exec();
}
