#pragma once

#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QFile>
#include <QTextStream>

#include <QSqlDriver>
/**
 * @brief The MMDatabase class
 *
 * Database utilities class
 */
class MMDatabase
{
public:
    static bool ConnectToDatabase();
    static bool SetupDatabase();
    static QSqlQuery Query(QString sql, QVector<QString> const &values = {});
    static int LastId();
private:
    /**
     * @brief connected
     *
     * If the database is connected
     */
    static bool connected;

    /**
     * @brief lastInsertId
     *
     * Last inserted id
     */
    static int lastInsertId;

    /**
     * @brief Driver
     *
     * The used driver for this software
     */
    static const QString Driver;

    /**
     * @brief DatabaseName
     *
     * The database name for this software
     */
    static const QString DatabaseName;

    static void InsertAllMandatoryTables();
};
