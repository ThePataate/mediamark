#include "media.h"

int Media::getId() const
{
    return id;
}

const QString &Media::getName() const
{
    return name;
}

void Media::setName(const QString &newName)
{
    name = newName;
}

const QDate &Media::getRelease_date() const
{
    return release_date;
}

void Media::setRelease_date(const QDate &newRelease_date)
{
    release_date = newRelease_date;
}

const QString &Media::getSynopsis() const
{
    return synopsis;
}

void Media::setSynopsis(const QString &newSynopsis)
{
    synopsis = newSynopsis;
}

const QDateTime &Media::getCreation_datetime() const
{
    return creation_datetime;
}

const QDateTime &Media::getUpdate_datetime() const
{
    return update_datetime;
}

const QDateTime &Media::getArchive_datetime() const
{
    return archive_datetime;
}

void Media::Archive()
{
    archive_datetime = QDateTime::currentDateTime();

    MMDatabase::Query("UPDATE media SET archive_datetime = ? WHERE id = ?", {archive_datetime.toString(Qt::ISODate), QString::number(id)});
}

/**
 * Returns the maximum progression of a media via it's id
 *
 * @brief Media::GetMax
 * @param id
 *
 * @return max progression
 */
int Media::GetMax(int id)
{
    QSqlQuery query = MMDatabase::Query("SELECT length FROM movie WHERE media_id = ?", {QString::number(id)});
    if(query.next())
    {
        return query.value(0).toInt();
    }
    query = MMDatabase::Query("SELECT number_pages FROM book WHERE media_id = ?", {QString::number(id)});
    if(query.next())
    {
        return query.value(0).toInt();
    }
    /*
     * TODO number of episodes from a tv_show season
    query = MMDatabase::Query("SELECT length FROM movie WHERE media_id = ?", {QString::number(id)});
    if(query.next())
    {
        return query.value(0).toInt();
    }
    */
    return 0;
}

/**
 * Returns the type of a media based on it's id
 *
 * @brief Media::GetTypeById
 * @param id
 * @return MediaType
 */
MediaType Media::GetTypeById(int id)
{
    QSqlQuery query = MMDatabase::Query("SELECT media_id FROM movie WHERE media_id = ?", {QString::number(id)});
    if(query.next())
    {
        return MediaType::MovieType;
    }
    query = MMDatabase::Query("SELECT media_id FROM tv_show WHERE media_id = ?", {QString::number(id)});
    if(query.next())
    {
        return MediaType::Tv_showType;
    }
    query = MMDatabase::Query("SELECT media_id FROM book WHERE media_id = ?", {QString::number(id)});
    if(query.next())
    {
        return MediaType::BookType;
    }

    //Default media type in case of errors
    return MediaType::MovieType;
}
