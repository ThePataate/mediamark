#include "userlistwidget.h"

/**
 * Constructor
 *
 * @brief UserListWidget::UserListWidget
 * @param id
 * @param parent
 */
UserListWidget::UserListWidget(int id, QWidget *parent) : QWidget(parent)
{
    //Manipulation to avoid multiple pointers being created and not destroyed on page change
    //TODO Find a better way to do this
    QVBoxLayout *vLayout = new QVBoxLayout;
    if(parent->layout() != nullptr)
    {
        delete parent->layout();
    }
    parent->setLayout(vLayout);
    vLayout->addWidget(this);


    item = new UserListItem(id);
    item->GetForm(this);
}

/**
 * Saves data from form
 *
 * @brief UserListWidget::SubmitForm
 */
void UserListWidget::SubmitForm()
{
    item->UpdateFromForm();
    item->Save();

    emit Saved();
}
