#include "mmmainwindow.h"

/**
 * Constructor
 *
 * @brief MMMainWindow::MMMainWindow
 * @param parent
 */
MMMainWindow::MMMainWindow(QWidget *parent) : QMainWindow(parent)
{
    InitProperties();

    resize(baseSize);

    InitTabs();
}

/**
 * Initializes main window properties
 *
 * @brief MMMainWindow::InitProperties
 */
void MMMainWindow::InitProperties()
{
    baseSize = QSize(1000, 500);

    //Menu bar
    menuBar = new QMenuBar;
    setMenuBar(menuBar);
    QMenu *mediasMenu = menuBar->addMenu("Medias");
    QAction *newMedia = new QAction("New Media");
    mediasMenu->addAction(newMedia);
    QObject::connect(newMedia, SIGNAL(triggered()), this, SLOT(GoToNewMedia()));

    //Tool bar
    QToolBar *toolbar = new QToolBar;
    toolbar->setMovable(false);
    QAction *backToListAction = new QAction("Back to lists");
    toolbar->addAction(backToListAction);
    searchbar = new QLineEdit;
    searchbar->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    toolbar->addWidget(searchbar);
    addToolBar(toolbar);

    QObject::connect(backToListAction, SIGNAL(triggered()), this, SLOT(GoToList()));
    QObject::connect(searchbar, SIGNAL(returnPressed()), this, SLOT(Search()));


    //Main widgets
    mainWidget = new QWidget;
    mainWidgetVLayout = new QVBoxLayout;
    setCentralWidget(mainWidget);
    mainWidget->setLayout(mainWidgetVLayout);
    holder = new QWidget(mainWidget);

    stackLayout = new QStackedLayout;
    tabWidget = new QTabWidget;
    stackLayout->addWidget(tabWidget);
    mediaWidgetHolder = new QWidget(holder);
    stackLayout->addWidget(mediaWidgetHolder);

    mainWidgetVLayout->addLayout(stackLayout);
}

/**
 * Initializes the media lists tabs
 *
 * @brief MMMainWindow::InitTabs
 */
void MMMainWindow::InitTabs()
{
//    MediaListWidget *movieListWidget = new MediaListWidget(MediaType::MovieType, holder);
    MediaTableWidget *movieListWidget = new MediaTableWidget(MediaTableWidget::Movie, "", holder);
    MediaTableWidget *tvShowListWidget = new MediaTableWidget(MediaTableWidget::Tv_show, "", holder);
    MediaTableWidget *bookListWidget = new MediaTableWidget(MediaTableWidget::Book, "", holder);
    MediaTableWidget *userListWidget = new MediaTableWidget(MediaTableWidget::UserList, "", holder);
//    MediaListWidget *tvShowListWidget = new MediaListWidget(MediaType::Tv_showType, holder);
//    MediaListWidget *bookListWidget = new MediaListWidget(MediaType::BookType, holder);
//    UserListTableWidget *userListWidget = new UserListTableWidget;
    MediaTableWidget *searchTab = new MediaTableWidget(MediaTableWidget::MediaSearch, "", holder);

    tabWidget->addTab(movieListWidget, "Movies");
    tabWidget->addTab(tvShowListWidget, "Tv Shows");
    tabWidget->addTab(bookListWidget, "Books");
    tabWidget->addTab(userListWidget, "List");
    tabWidget->addTab(searchTab, "Search");
    tabWidget->setTabEnabled(4, false);
    tabWidget->setTabVisible(4, false);

    QObject::connect(tabWidget, SIGNAL(currentChanged(int)), this, SLOT(UpdateCurrentList()));

    QObject::connect(movieListWidget, SIGNAL(UpdateButtonClicked(int)), this, SLOT(GoToUpdatePage(int)));
    QObject::connect(tvShowListWidget, SIGNAL(UpdateButtonClicked(int)), this, SLOT(GoToUpdatePage(int)));
    QObject::connect(bookListWidget, SIGNAL(UpdateButtonClicked(int)), this, SLOT(GoToUpdatePage(int)));
    QObject::connect(userListWidget, SIGNAL(UpdateButtonClicked(int)), this, SLOT(GoToUpdatePage(int)));
    QObject::connect(searchTab, SIGNAL(UpdateButtonClicked(int)), this, SLOT(GoToUpdatePage(int)));

    QObject::connect(movieListWidget, SIGNAL(AddToListButtonClicked(int)), this, SLOT(AddToList(int)));
    QObject::connect(tvShowListWidget, SIGNAL(AddToListButtonClicked(int)), this, SLOT(AddToList(int)));
    QObject::connect(bookListWidget, SIGNAL(AddToListButtonClicked(int)), this, SLOT(AddToList(int)));
    QObject::connect(searchTab, SIGNAL(AddToListButtonClicked(int)), this, SLOT(AddToList(int)));
}

/**
 * Setups the form page of the specified media type with id
 *
 * @brief MMMainWindow::GoToFormPage
 * @param type
 * @param id
 */
void MMMainWindow::GoToFormPage(MediaType type, int id)
{
    if(stackLayout->widget(1)->children().size())
    {
        QObject *previousMediaWidgetPtr = stackLayout->widget(1)->children()[0];
        delete previousMediaWidgetPtr;
    }

    mediaWidget = new MediaWidget(type, id, mediaWidgetHolder);

    ChangePage(1);

    QObject::connect(mediaWidget, SIGNAL(Saved()), this, SLOT(GoToList()));
}

/**
 * Changes the main displayed window
 *
 * @brief MMMainWindow::ChangePage
 * @param index
 */
void MMMainWindow::ChangePage(int index)
{
    if(index >= 0 && index < stackLayout->count())
    {
        stackLayout->setCurrentIndex(index);
    }
}

/**
 * Reloads the data from the media lists
 *
 * @brief MMMainWindow::UpdateCurrentList
 */
void MMMainWindow::UpdateCurrentList()
{
    ((MediaTableWidget*)tabWidget->currentWidget())->Refresh();
    if(tabWidget->currentIndex() != 4)
    {
        tabWidget->setTabEnabled(4, false);
        tabWidget->setTabVisible(4, false);
    }
}

/**
 * Returns to the lists view
 *
 * @brief MMMainWindow::GoToList
 */
void MMMainWindow::GoToList()
{
    ChangePage(0);
    UpdateCurrentList();
}

/**
 * Opens a dialog and then opens the corresponding form
 *
 * @brief MMMainWindow::GoToNewMedia
 */
void MMMainWindow::GoToNewMedia()
{
    QInputDialog dialog = QInputDialog();
    QStringList items;
    items << "Movie" << "Tv Show" << "Book";
    bool ok;

    //Opens the form to let user select what kind of media to add
    QString item = dialog.getItem(this, "What type of media do you want to add", "Media Type:", items, tabWidget->currentIndex(), false, &ok);

    //If dialog is valid
    if(ok && !item.isEmpty())
    {
        if(item == "Movie")
        {
            GoToFormPage(MediaType::MovieType);
        }else if(item == "Tv Show")
        {
            GoToFormPage(MediaType::Tv_showType);
        }else if(item == "Book")
        {
            GoToFormPage(MediaType::BookType);
        }
    }

}

/**
 * Go to the form page of a media
 *
 * @brief MMMainWindow::GoToUpdatePage
 * @param id
 */
void MMMainWindow::GoToUpdatePage(int id)
{
    MediaType type = MediaType::MovieType;

    switch (tabWidget->currentIndex()) {
        case 0 :
            type = MediaType::MovieType;
            break;
        case 1:
            type = MediaType::Tv_showType;
            break;
        case 2:
            type = MediaType::BookType;
            break;
        case 3:
            GoToListItemPage(id);
            return;
            break;
        case 4:
            type = Media::GetTypeById(id);
            break;
    }

    GoToFormPage(type, id);
}

/**
 * Go to the form page of a list item
 *
 * @brief MMMainWindow::GoToListItemPage
 * @param id
 */
void MMMainWindow::GoToListItemPage(int id)
{
    if(stackLayout->widget(1)->children().size())
    {
        QObject *previousWidgetPtr = stackLayout->widget(1)->children()[0];
        delete previousWidgetPtr;
    }

    mediaWidget = new UserListWidget(id, mediaWidgetHolder);

    ChangePage(1);

    QObject::connect(mediaWidget, SIGNAL(Saved()), this, SLOT(GoToList()));
}

/**
 * Adds to user list selected media
 *
 * @brief MMMainWindow::AddToList
 * @param id
 */
void MMMainWindow::AddToList(int id)
{
    QMessageBox msgBox;

    //First we check if this media is not already in the list
    if(UserListItem::AlreadyInList(id))
    {
        msgBox.setText("This media is already in your list");
        msgBox.exec();
        GoToListItemPage(id);
        return;
    }

    //We ask confirmation to add to list
    msgBox.setText("Add this to your list");
    msgBox.setInformativeText("Do you want to add this media to your list ?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);

    int result = msgBox.exec();

    UserListItem item = UserListItem(id);
    switch (result) {
        case QMessageBox::Save:
            item.setStatus(UserListStatus::Planned);
            item.setProgression(0);
            item.Save();
            break;
        case QMessageBox::Cancel:
        default:
            //Nothing happens
            break;
    }
}

/**
 * Search system based on filter typed in toolbar
 *
 * @brief MMMainWindow::Search
 */
void MMMainWindow::Search()
{
    QString filter = searchbar->text();

    if(filter.isEmpty() || filter.isNull())
    {
        qInfo() << "Empty research";
        return;
    }

    ChangePage(0);
    tabWidget->setTabEnabled(4, true);
    tabWidget->setTabVisible(4, true);
    tabWidget->setCurrentIndex(4);
    ((MediaTableWidget*)tabWidget->currentWidget())->SetFilter(filter);
}
