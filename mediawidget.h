#pragma once

#include <QWidget>
#include <QVBoxLayout>

#include <enums.h>
#include <movie.h>
#include <book.h>
#include <tv_show.h>

class MediaWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MediaWidget(MediaType type, int id = 0, QWidget *parent = nullptr);
protected:
    /**
     * @brief media
     */
    Media *media;
public slots:
    void SaveForm();
signals:
    /**
     * The form has been submited
     *
     * @brief Saved
     */
    void Saved();
};
