#pragma once

#include <QWidget>
#include <QDate>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QSpinBox>
#include <QDateEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QComboBox>

#include <media.h>

class UserListItemFormWidget : public QWidget
{
    Q_OBJECT
public:
    explicit UserListItemFormWidget(QWidget *parent = nullptr);

    void SetStatus(QString status);
    void SetProgression(int progression);
    void SetProgressionMax(int max);
    void SetBeginDate(QDate date);
    void SetBeginDateCheck(bool state);
    void SetEndDate(QDate date);
    void SetEndDateCheck(bool state);

    QString GetStatus() const;
    int GetProgression() const;
    QDate GetBeginDate() const;
    QDate GetEndDate() const;

protected:
    QComboBox *statusEdit;
    QSpinBox *progressionEdit;
    QDateEdit *beginDateEdit;
    QCheckBox *beginDateCheck;
    QDateEdit *endDateEdit;
    QCheckBox *endDateCheck;

public slots:
    void ChangeStatus(const QString &status);
    void SetBeginDisabled(int state);
    void SetEndDisabled(int state);
    void FormSubmit();

signals:
    /**
     * Signal emited when this form is submited
     *
     * @brief FormSubmited
     */
    void FormSubmited();
};
