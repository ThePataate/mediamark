#include "mediaquerymodel.h"

/**
 * Constructor
 *
 * @brief MediaQueryModel::MediaQueryModel
 * @param type_
 * @param parent
 */
MediaQueryModel::MediaQueryModel(MediaType type_, QObject *parent) : QSqlQueryModel(parent), type(type_)
{
    Refresh();

    //Setting the common headers
    setHeaderData(0, Qt::Horizontal, "Name");
    setHeaderData(1, Qt::Horizontal, "Name");
    setHeaderData(2, Qt::Horizontal, "Synopsis");
    setHeaderData(4, Qt::Horizontal, "Director");
    setHeaderData(5, Qt::Horizontal, "Release Date");
    setHeaderData(6, Qt::Horizontal, "Update");
    setHeaderData(7, Qt::Horizontal, "Add To List");

    //Setting the specific headers
    switch (type) {
        case MediaType::MovieType :
            setHeaderData(3, Qt::Horizontal, "Length");
            break;
        case MediaType::Tv_showType :
            setHeaderData(3, Qt::Horizontal, "Season Number");
            setHeaderData(6, Qt::Horizontal, "End Date");
            setHeaderData(7, Qt::Horizontal, "Update");
            setHeaderData(8, Qt::Horizontal, "Add To List");
            break;
        case MediaType::BookType :
            setHeaderData(3, Qt::Horizontal, "Pages");
            setHeaderData(4, Qt::Horizontal, "Author");
            setHeaderData(7, Qt::Horizontal, "Update");
            setHeaderData(8, Qt::Horizontal, "Add To List");
            break;
        case MediaType::Tv_show_episodeType :
            break;
    }
}

/**
 * @brief MediaQueryModel::data
 * @param index
 * @param role
 * @return formated value
 */
QVariant MediaQueryModel::data(const QModelIndex &index, int role) const
{
    QVariant value = QSqlQueryModel::data(index, role);
    if(role == Qt::DisplayRole && value.isValid())
    {
        //Formats the movie length to a 00h 00m string
        if(index.column() == 3 && type == MediaType::MovieType)
        {
            return QString::number(value.toInt() / 60) + "h" + QString::number(value.toInt() % 60) + "m";
        }
        if(index.column() == 5)
        {
            //TODO add a local feature (french/english date formats)
            return value.toDate().toString(Qt::ISODate);
        }
        //Setting the double click cells display value
        if((index.column() == 6 && type == MediaType::MovieType) || (index.column() >= 7))
        {
            return "=>";
        }
    }

    return value;
}

/**
 * Refreshs this model's data by setting the query
 *
 * @brief MediaQueryModel::Refresh
 */
void MediaQueryModel::Refresh()
{
    QString sql;

    switch (type) {
        case MediaType::MovieType :
            sql = "SELECT id, name, synopsis, length, director, release_date, media_id, '' FROM movie INNER JOIN media ON movie.media_id = media.id";
            break;
        case MediaType::Tv_showType :
            sql = "SELECT id, name, synopsis, season_number, director, release_date, end_date, media_id, '' FROM tv_show INNER JOIN media ON tv_show.media_id = media.id";
            break;
        case MediaType::BookType :
            sql = "SELECT id, name, synopsis, number_pages, author, release_date, ISBN, media_id, '' FROM book INNER JOIN media ON book.media_id = media.id";
            break;
        case MediaType::Tv_show_episodeType :
            sql = "SELECT * FROM media";
            break;
    }

    //Setting this model query based on its MediaType
    setQuery(sql);
}
