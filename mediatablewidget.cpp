#include "mediatablewidget.h"

/**
 * Constructor
 *
 * @brief MediaTableWidget::MediaTableWidget
 * @param type
 * @param filter_
 * @param parent
 */
MediaTableWidget::MediaTableWidget(ViewType type, QString filter_, QWidget *parent) : QWidget(parent), viewType(type), filter(filter_)
{
    QHBoxLayout *hLayout = new QHBoxLayout;

    //Initialize model based on viewtype
    switch (viewType) {
        case ViewType::Movie:
            model = new MediaQueryModel(MediaType::MovieType);
            break;
        case ViewType::Tv_show:
            model = new MediaQueryModel(MediaType::Tv_showType);
            break;
        case ViewType::Book:
            model = new MediaQueryModel(MediaType::BookType);
            break;
        case ViewType::MediaSearch:
            model = new MediaSearchQueryModel(filter);
            break;
        case ViewType::UserList:
            model = new UserListQueryModel;
            break;
    }

    tableView = new QTableView(this);
    tableView->setModel(model);
    tableView->setCornerButtonEnabled(false);
    tableView->setSortingEnabled(false);
    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    //Need to implement a proper sorting, does nothing for now
    tableView->sortByColumn(1, Qt::AscendingOrder);

    //Id column (used to keep track of row's corresponding media id)
    tableView->hideColumn(0);

    hLayout->addWidget(tableView);
    setLayout(hLayout);


    QObject::connect(tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(GoToUpdatePage(QModelIndex)));
}

/**
 * Destructor
 *
 * @brief MediaTableWidget::~MediaTableWidget
 */
MediaTableWidget::~MediaTableWidget()
{
    delete tableView;
    delete model;
}

/**
 * Checks the double click of cells and emits corresponding signal
 *
 * @brief GoToUpdatePage
 * @param index
 */
void MediaTableWidget::GoToUpdatePage(const QModelIndex &index)
{
    switch (viewType) {
        case ViewType::Movie:
        case ViewType::Tv_show:
        case ViewType::Book:
            //Clicked the update cell
            if((index.column() == 6 && viewType == ViewType::Movie) || (index.column() == 7 && viewType != ViewType::Movie))
            {
                emit UpdateButtonClicked(model->index(index.row(), 0).data().toInt());
            }
            //Clicked the add to list cell
            if((index.column() == 7 && viewType == ViewType::Movie) || (index.column() == 8))
            {
                emit AddToListButtonClicked(model->index(index.row(), 0).data().toInt());
            }
            break;
        case ViewType::MediaSearch:
            //Clicked the update cell
            if(index.column() == 3)
            {
                emit UpdateButtonClicked(model->index(index.row(), 0).data().toInt());
            }
            //Clicked the add to list cell
            if(index.column() == 4)
            {
                emit AddToListButtonClicked(model->index(index.row(), 0).data().toInt());
            }
            break;
        case ViewType::UserList:
            //Clicked the update cell
            if(index.column() == 6)
            {
                emit UpdateButtonClicked(model->index(index.row(), 0).data().toInt());
            }
            break;
    }

}

/**
 * Refreshs this tableview data
 *
 * @brief MediaTableWidget::Refresh
 */
void MediaTableWidget::Refresh()
{
    switch (viewType) {
        case ViewType::Movie:
        case ViewType::Tv_show:
        case ViewType::Book:
            ((MediaQueryModel*)tableView->model())->Refresh();
            break;
        case ViewType::MediaSearch:
            ((MediaSearchQueryModel*)tableView->model())->SetCustomQuery(filter);
            break;
        case ViewType::UserList:
            ((UserListQueryModel*)tableView->model())->Refresh();
            break;
    }
}

/**
 * @brief MediaTableWidget::SetFilter
 * @param filter_
 */
void MediaTableWidget::SetFilter(const QString &filter_)
{
    filter = filter_;
    Refresh();
}
