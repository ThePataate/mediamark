#pragma once

#include <QSqlQueryModel>
#include <QDate>

#include <enums.h>

/**
 * @brief The MediaQueryModel class
 *
 * Child of QSqlQueryModel used to store media relative data
 */
class MediaQueryModel : public QSqlQueryModel
{
public:
    explicit MediaQueryModel(MediaType type, QObject *parent = nullptr);
    QVariant data(const QModelIndex &index, int role) const override;
    void Refresh();
protected:
    /**
     * @brief type
     *
     * The MediaType that this MediaQueryModel describes
     */
    MediaType type;
};
