#include "userlistitem.h"

/**
 * Constructor
 *
 * @brief UserListItem::UserListItem
 * @param media_id_
 */
UserListItem::UserListItem(int media_id_)
{
    media_id = media_id_;

    //Fetches data from database
    QSqlQuery query = MMDatabase::Query("SELECT * FROM user_list WHERE media_id = ?", {QString::number(media_id)});

    if(query.next())
    {
        status = query.value(1).toString();
        progression = query.value(2).toInt();
        begin_date = query.value(3).toDate();
        end_date = query.value(4).toDate();

        exists = true;
    }
}

/**
 * Destructor
 *
 * @brief UserListItem::~UserListItem
 */
UserListItem::~UserListItem()
{
    delete form;
}

/**
 * Saves data
 *
 * @brief UserListItem::Save
 */
void UserListItem::Save()
{
    if(!exists)
    {
        MMDatabase::Query(
            "INSERT INTO user_list(media_id, status, progression) VALUES (?,?,?)",
            {
                QString::number(media_id),
                status,
                QString::number(progression)
            }
        );
        exists = true;
    }

    MMDatabase::Query(
        "UPDATE user_list set status = ?, progression = ?, begin_date = ?, end_date = ? WHERE media_id = ?",
        {
            status,
            QString::number(progression),
            begin_date.isNull() ? "" : begin_date.toString(Qt::ISODate),
            end_date.isNull() ? "" : end_date.toString(Qt::ISODate),
            QString::number(media_id)
        }
    );

}

/**
 * Generates a form to update/display this item data
 *
 * @brief UserListItem::GetForm
 * @param w
 */
void UserListItem::GetForm(QWidget *w)
{

    form = new UserListItemFormWidget(w);

    form->SetStatus(status);
    form->SetProgressionMax(Media::GetMax(media_id));
    form->SetProgression(progression);
    form->SetBeginDate(begin_date);
    form->SetBeginDateCheck(begin_date.isNull());
    form->SetEndDate(end_date);
    form->SetEndDateCheck(end_date.isNull());

    QObject::connect(form, SIGNAL(FormSubmited()), w, SLOT(SubmitForm()));
}

/**
 * Get data from the form and sets it
 *
 * @brief UserListItem::UpdateFromForm
 */
void UserListItem::UpdateFromForm()
{
    status = form->GetStatus();
    progression = form->GetProgression();
    begin_date = form->GetBeginDate();
    end_date = form->GetEndDate();
}

/**
 * Checks if specified media is not already in user's list
 *
 * @brief UserListItem::AlreadyInList
 * @param media_id
 * @return bool
 */
bool UserListItem::AlreadyInList(int media_id)
{
    QSqlQuery query = MMDatabase::Query("SELECT media_id FROM user_list WHERE media_id = ?", {QString::number(media_id)});

    return query.next();
}

/* GETTERS & SETTERS */

const QString &UserListItem::getStatus() const
{
    return status;
}

void UserListItem::setStatus(UserListStatus newStatus)
{
    switch (newStatus) {
        case UserListStatus::Started:
            status = "Started";
            break;
        case UserListStatus::Finished:
            status = "Finished";
            break;
        case UserListStatus::Planned:
            status = "Planned";
            break;
        case UserListStatus::Dropped:
            status = "Dropped";
            break;
    }
}

int UserListItem::getProgression() const
{
    return progression;
}

void UserListItem::setProgression(int newProgression)
{
    progression = newProgression;
}

