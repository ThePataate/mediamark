#pragma once

#include <QFormLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QDateEdit>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QDateTime>

#include "media.h"

class Book : public Media
{
public:
    Book(int id);

protected:
    QString author;
    QString isbn;
    int number_pages;

    // Media interface
public:
    void Save() override;
    void GetForm(QWidget *w) override;
    void UpdateFromForm(QWidget *w) override;
};
