#include "movie.h"

Movie::Movie(int id_)
{
    id = id_;

    //We initialize properties
    name = "";
    synopsis = "";
    release_date = QDate::currentDate();
    director = "";
    length = 0;

    if(id)
    {
        QSqlQuery query = MMDatabase::Query("SELECT name, synopsis, release_date, director, length FROM movie INNER JOIN media ON movie.media_id = media.id WHERE id = ?", {QString::number(id)});

        if(query.next())
        {
            name = query.value(0).toString();
            synopsis = query.value(1).toString();
            release_date = query.value(2).toDate();
            director = query.value(3).toString();
            length = query.value(4).toInt();
        }
    }
}

Movie::Movie(QMap<QString, QVariant> values)
{
    for(auto it = values.begin(); it != values.end(); it++)
    {
        if(it.key() == "name"){
            name = it.value().toString();
        }else if(it.key() == "synopsis"){
           synopsis = it.value().toString();
        }else if(it.key() == "release_date"){
            release_date = it.value().toDate();
        }else if(it.key() == "director"){
            director = it.value().toString();
        }else if (it.key() == "length"){
            length = it.value().toInt();
        }
    }
}

void Movie::Save()
{
    if(id)
    {
        MMDatabase::Query(
            "UPDATE media SET name = ?, synopsis = ?, release_date = ?, update_datetime = ? WHERE id = ?",
            {
              name,
              synopsis,
              release_date.toString(Qt::ISODate),
              QDateTime::currentDateTime().toString(Qt::ISODate),
              QString::number(id)
            }
        );

        MMDatabase::Query(
            "UPDATE movie SET director = ?, length = ? WHERE media_id = ?",
            {
                director,
                QString::number(length),
                QString::number(id)
            }
        );
    }else
    {
        MMDatabase::Query(
            "INSERT INTO media (name, synopsis, release_date, creation_datetime) VALUES (?,?,?,?)",
            {
                name,
                synopsis,
                release_date.toString(Qt::ISODate),
                QDateTime::currentDateTime().toString(Qt::ISODate),
            }
        );
        id = MMDatabase::LastId();
        MMDatabase::Query(
            "INSERT INTO movie (media_id, director, length) VALUES (?,?,?)",
            {
                QString::number(id),
                director,
                QString::number(length)
            }
        );
    }
}

void Movie::GetForm(QWidget *w)
{
    QVBoxLayout *vLayout = new QVBoxLayout;

    QLabel *titleLabel = new QLabel;
    titleLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    QFont font = QFont();
    font.setPointSize(25);
    font.setBold(true);
    titleLabel->setFont(font);
    titleLabel->setText("Movie");

    vLayout->addWidget(titleLabel, 0, Qt::AlignCenter);

    QFormLayout *layout = new QFormLayout;
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout->setFormAlignment(Qt::AlignHCenter|Qt::AlignTop);
    layout->setVerticalSpacing(50);
    layout->setContentsMargins(10, 10, 10, 10);


    QLineEdit *nameEdit = new QLineEdit(name);
    QTextEdit *synopsisEdit = new QTextEdit(synopsis);
    QTimeEdit *lengthEdit = new QTimeEdit(QTime(0, 0).addSecs(length * 60));
    QLineEdit *directorEdit = new QLineEdit(director);
    QDateEdit *dateEdit = new QDateEdit(release_date);

    layout->addRow("Name", nameEdit);
    layout->addRow("Synopsis", synopsisEdit);
    layout->addRow("Length", lengthEdit);
    layout->addRow("Director", directorEdit);
    layout->addRow("Release Date", dateEdit);

    vLayout->addLayout(layout);

    QPushButton *saveButton = new QPushButton;
    saveButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    saveButton->setText("Save");

    vLayout->addWidget(saveButton, 0, Qt::AlignHCenter);

    w->setLayout(vLayout);

    QObject::connect(saveButton, SIGNAL(clicked()), w, SLOT(SaveForm()));
}

void Movie::UpdateFromForm(QWidget *w)
{
    name = w->children().at(3)->property("text").toString();
    synopsis = w->children().at(5)->property("plainText").toString();
    length = w->children().at(7)->property("time").toTime().hour() * 60 + w->children().at(7)->property("time").toTime().minute();
    director = w->children().at(9)->property("text").toString();
    release_date = w->children().at(11)->property("date").toDate();
}
