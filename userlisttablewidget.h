#pragma once

#include <QWidget>
#include <QTableView>
#include <QHBoxLayout>
#include <QHeaderView>

#include <QPushButton>

#include <userlistquerymodel.h>

class UserListTableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit UserListTableWidget(QWidget *parent = nullptr);

protected:
    /**
     * @brief model
     *
     * QSqlQueryModel child class used as template model for the tableview
     */
    UserListQueryModel *model;

    /**
     * @brief tableView
     *
     * TableView used to display data
     */
    QTableView *tableView;

public slots:
    void GoToUpdatePage(const QModelIndex& index);
    void Refresh();

signals:
    /**
     * Signal emitted when update is clicked
     *
     * @brief UpdateButtonClicked
     * @param id
     */
    void UpdateButtonClicked(int id);
};
