SOURCES += \
    book.cpp \
    main.cpp \
    media.cpp \
    medialistwidget.cpp \
    mediaquerymodel.cpp \
    mediasearchquerymodel.cpp \
    mediatablewidget.cpp \
    mediawidget.cpp \
    mmdatabase.cpp \
    mmmainwindow.cpp \
    movie.cpp \
    tv_show.cpp \
    userlistitem.cpp \
    userlistitemformwidget.cpp \
    userlistquerymodel.cpp \
    userlisttablewidget.cpp \
    userlistwidget.cpp
    QT += widgets sql

HEADERS += \
    book.h \
    enums.h \
    media.h \
    medialistwidget.h \
    mediaquerymodel.h \
    mediasearchquerymodel.h \
    mediatablewidget.h \
    mediawidget.h \
    mmdatabase.h \
    mmmainwindow.h \
    movie.h \
    tv_show.h \
    userlistitem.h \
    userlistitemformwidget.h \
    userlistquerymodel.h \
    userlisttablewidget.h \
    userlistwidget.h
