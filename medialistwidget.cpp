#include "medialistwidget.h"

/**
 * Constructor
 * @brief MediaListWidget::MediaListWidget
 * @param type
 * @param parent
 */
MediaListWidget::MediaListWidget(MediaType type_, QWidget *parent) : QWidget(parent), type(type_)
{
    QHBoxLayout *hLayout = new QHBoxLayout;

    model = new MediaQueryModel(type);

    tableView = new QTableView(this);
    tableView->setModel(model);
    tableView->setCornerButtonEnabled(false);
    tableView->setSortingEnabled(false);
    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    //Need to implement a proper sorting, does nothing for now
    tableView->sortByColumn(1, Qt::AscendingOrder);

    //Id column (used to keep track of row's corresponding media id)
    tableView->hideColumn(0);

    hLayout->addWidget(tableView);
    setLayout(hLayout);

    QObject::connect(tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(GoToUpdatePage(QModelIndex)));
}

/**
 * Checks the double click of cells and emits corresponding signal
 *
 * @brief GoToUpdatePage
 * @param index
 */
void MediaListWidget::GoToUpdatePage(const QModelIndex &index)
{
    //Clicked the update cell
    if((index.column() == 6 && type == MediaType::MovieType) || (index.column() == 7 && type != MediaType::MovieType))
    {
        emit UpdateButtonClicked(model->index(index.row(), 0).data().toInt());
    }

    //Clicked the add to list cell
    if((index.column() == 7 && type == MediaType::MovieType) || (index.column() == 8))
    {
        emit AddToListButtonClicked(model->index(index.row(), 0).data().toInt());
    }

}

/**
 * Refreshs this tableview data
 *
 * @brief MediaListWidget::Refresh
 */
void MediaListWidget::Refresh()
{
    ((MediaQueryModel*)tableView->model())->Refresh();
}
