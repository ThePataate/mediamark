#include "userlistitemformwidget.h"

/**
 * Constructor
 *
 * @brief UserListItemFormWidget::UserListItemFormWidget
 * @param parent
 */
UserListItemFormWidget::UserListItemFormWidget(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *vLayout = new QVBoxLayout;

    QLabel *titleLabel = new QLabel;
    titleLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    QFont font = QFont();
    font.setPointSize(25);
    font.setBold(true);
    titleLabel->setFont(font);
    titleLabel->setText("Item");

    vLayout->addWidget(titleLabel, 0, Qt::AlignCenter);

    QFormLayout *layout = new QFormLayout;
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout->setFormAlignment(Qt::AlignHCenter|Qt::AlignTop);
    layout->setVerticalSpacing(50);
    layout->setContentsMargins(10, 10, 10, 10);

    //Status
    statusEdit = new QComboBox;
    QStringList items;
    items << "Started" << "Finished" << "Planned" << "Dropped";
    statusEdit->addItems(items);
    statusEdit->setEditable(false);

    QObject::connect(statusEdit, SIGNAL(currentTextChanged(QString)), this, SLOT(ChangeStatus(QString)));

    //Progression
    progressionEdit = new QSpinBox;

    //Begin Date
    QWidget *beginDateHolder = new QWidget;
    QHBoxLayout *beginDateHolderLayout = new QHBoxLayout;
    beginDateEdit = new QDateEdit;
    beginDateEdit->setSpecialValueText(" ");
    beginDateEdit->setMinimumDate(QDate(1950, 1, 1));
    beginDateCheck = new QCheckBox("Unknown Date");
    beginDateHolderLayout->addWidget(beginDateEdit);
    beginDateHolderLayout->addWidget(beginDateCheck);
    beginDateHolder->setLayout(beginDateHolderLayout);

    QObject::connect(beginDateCheck, SIGNAL(stateChanged(int)), this, SLOT(SetBeginDisabled(int)));

    //End date
    QWidget *endDateHolder = new QWidget;
    QHBoxLayout *endDateHolderLayout = new QHBoxLayout;
    endDateEdit = new QDateEdit();
    endDateEdit->setSpecialValueText(" ");
    endDateEdit->setMinimumDate(QDate(1950, 1, 1));
    endDateCheck = new QCheckBox("Unknown Date");
    endDateHolderLayout->addWidget(endDateEdit);
    endDateHolderLayout->addWidget(endDateCheck);
    endDateHolder->setLayout(endDateHolderLayout);

    QObject::connect(endDateCheck, SIGNAL(stateChanged(int)), this, SLOT(SetEndDisabled(int)));

    layout->addRow("Status", statusEdit);
    layout->addRow("Progression", progressionEdit);
    layout->addRow("Begin Date", beginDateHolder);
    layout->addRow("End Date", endDateHolder);

    vLayout->addLayout(layout);

    QPushButton *saveButton = new QPushButton;
    saveButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    saveButton->setText("Save");

    vLayout->addWidget(saveButton, 0, Qt::AlignHCenter);

    parent->setLayout(vLayout);

    QObject::connect(saveButton, SIGNAL(clicked()), this, SLOT(FormSubmit()));
}

/* GETTERS & SETTERS */

void UserListItemFormWidget::SetStatus(QString status)
{
    statusEdit->setCurrentText(status);
}
void UserListItemFormWidget::SetProgression(int progression)
{
    progressionEdit->setValue(progression);
}

void UserListItemFormWidget::SetProgressionMax(int max)
{
    progressionEdit->setMaximum(max);
}

void UserListItemFormWidget::SetBeginDate(QDate date)
{
    beginDateEdit->setDate(date);
}

void UserListItemFormWidget::SetBeginDateCheck(bool state)
{
    beginDateEdit->setDisabled(state);
    beginDateCheck->setChecked(state);
}

void UserListItemFormWidget::SetEndDate(QDate date)
{
    endDateEdit->setDate(date);
}

void UserListItemFormWidget::SetEndDateCheck(bool state)
{
    endDateEdit->setDisabled(state);
    endDateCheck->setChecked(state);
}

QString UserListItemFormWidget::GetStatus() const
{
    return statusEdit->currentText();
}

int UserListItemFormWidget::GetProgression() const
{
    return progressionEdit->value();
}

QDate UserListItemFormWidget::GetBeginDate() const
{
    if(beginDateCheck->isChecked())
    {
        return QDate();
    }
    return beginDateEdit->date();
}

QDate UserListItemFormWidget::GetEndDate() const
{
    if(endDateCheck->isChecked())
    {
        return QDate();
    }
    return endDateEdit->date();
}

/* SLOTS */

/**
 * Updates progression based on newly selected status
 *
 * @brief UserListItemFormWidget::ChangeStatus
 * @param status
 */
void UserListItemFormWidget::ChangeStatus(const QString &status)
{
    if(status == "Finished")
    {
        SetProgression(progressionEdit->maximum());
    }
    if(status == "Planned")
    {
        SetProgression(0);
    }
}

/**
 * Disables begin date selector when date is set to unknown
 *
 * @brief UserListItemFormWidget::SetBeginDisabled
 * @param state
 */
void UserListItemFormWidget::SetBeginDisabled(int state)
{
    beginDateEdit->setEnabled(!state);
    if(state)
    {
        beginDateEdit->setDate(QDate(1900, 1, 1));
    }
}

/**
 * Disables end date selector when date is set to unknown
 *
 * @brief UserListItemFormWidget::SetEndDisabled
 * @param state
 */
void UserListItemFormWidget::SetEndDisabled(int state)
{
    endDateEdit->setEnabled(!state);
    if(state)
    {
        endDateEdit->setDate(QDate(1900, 1, 1));
    }
}

/**
 * Save button has been clicked
 *
 * @brief UserListItemFormWidget::FormSubmit
 */
void UserListItemFormWidget::FormSubmit()
{
    emit FormSubmited();
}

