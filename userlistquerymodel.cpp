#include "userlistquerymodel.h"

/**
 * Constructor
 *
 * @brief UserListQueryModel::UserListQueryModel
 * @param parent
 */
UserListQueryModel::UserListQueryModel(QObject *parent) : QSqlQueryModel(parent)
{
    Refresh();

    setHeaderData(1, Qt::Horizontal, "Name");
    setHeaderData(2, Qt::Horizontal, "Status");
    setHeaderData(3, Qt::Horizontal, "Progression");
    setHeaderData(4, Qt::Horizontal, "Begin Date");
    setHeaderData(5, Qt::Horizontal, "End Date");
    setHeaderData(6, Qt::Horizontal, "Update");

}

/**
 * @brief UserListQueryModel::data
 * @param index
 * @param role
 * @return
 */
QVariant UserListQueryModel::data(const QModelIndex &index, int role) const
{
    QVariant value = QSqlQueryModel::data(index, role);
    if(role == Qt::DisplayRole && value.isValid())
    {
        if(index.column() == 3)
        {
            return value.toString() + "/" + QString::number(Media::GetMax(this->index(index.row(), 0).data().toInt()));
        }
        if(index.column() == 4 || index.column() == 5)
        {
            //TODO add a local feature (french/english date formats)
            return value.toDate().toString(Qt::ISODate);
        }
        if(index.column() == 6)
        {
            return "=>";
        }
    }

    return value;
}

/**
 * Refreshs this model's data by setting the query
 *
 * @brief UserListQueryModel::Refresh
 */
void UserListQueryModel::Refresh()
{
    this->setQuery("SELECT media_id, name, status, progression, begin_date, end_date, '' FROM user_list INNER JOIN media ON user_list.media_id = media.id");
}
