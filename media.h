#pragma once

#include <QWidget>
#include <QString>
#include <QDateTime>

#include <mmdatabase.h>
#include <enums.h>

class Media
{
public:
    virtual void Save() = 0;
    virtual void Archive();
    virtual void GetForm(QWidget *w) = 0;
    virtual void UpdateFromForm(QWidget *w) = 0;
    static int GetMax(int id);
    static MediaType GetTypeById(int id);

    int getId() const;

    const QString &getName() const;
    void setName(const QString &newName);

    const QDate &getRelease_date() const;
    void setRelease_date(const QDate &newRelease_date);

    const QString &getSynopsis() const;
    void setSynopsis(const QString &newSynopsis);

    const QDateTime &getCreation_datetime() const;

    const QDateTime &getUpdate_datetime() const;

    const QDateTime &getArchive_datetime() const;

protected:
    int id;
    QString name;
    QDate release_date;
    QString synopsis;
    QDateTime creation_datetime;
    QDateTime update_datetime;
    QDateTime archive_datetime;
};
