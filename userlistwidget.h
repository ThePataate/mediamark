#pragma once

#include <QWidget>
#include <QVBoxLayout>

#include <userlistitem.h>

class UserListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit UserListWidget(int id = 0, QWidget *parent = nullptr);

protected:
    /**
     * @brief item
     */
    UserListItem *item;

public slots:
    void SubmitForm();

signals:
    /**
     * Signal emitted when this form page is submited
     *
     * @brief Saved
     */
    void Saved();
};
