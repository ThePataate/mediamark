# MediaMark

Little software used to keep track of the user's progression while watching/reading different medias.


Base functionalities :
- Lists of medias (Movies, Tv shows or books)
- Search a specific media via different research topics (Name, release date, author, ...)
- Add a media to user's list
- Access user's list
- Modify an entry from the user's list (Change status, starting/end date, current progression)


Possible evolutions :
- Fetch medias data from APIs 
- Add a note to a media