#pragma once


#include <QFormLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QTimeEdit>
#include <QDateEdit>
#include <QLabel>
#include <QPushButton>
#include <QTime>
#include <QDateTime>

#include "media.h"

class Movie : public Media
{
public:
    Movie(int id);
    Movie(QMap<QString, QVariant> values);

protected:
    QString director;
    int length;

    // Media interface
public:
    void Save() override;
    void GetForm(QWidget *w) override;
    void UpdateFromForm(QWidget *w) override;
};

