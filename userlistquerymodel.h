#pragma once

#include <QSqlQueryModel>
#include <QDate>

#include <enums.h>
#include <media.h>

class UserListQueryModel : public QSqlQueryModel
{
public:
    explicit UserListQueryModel(QObject *parent = nullptr);

    void Refresh();

    // QAbstractItemModel interface
public:
    QVariant data(const QModelIndex &index, int role) const override;
};
