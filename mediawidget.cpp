#include "mediawidget.h"

MediaWidget::MediaWidget(MediaType type, int id, QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *vLayout = new QVBoxLayout;
    if(parent->layout() != nullptr)
    {
        delete parent->layout();
    }
    parent->setLayout(vLayout);
    vLayout->addWidget(this);

    switch (type) {
        case MediaType::MovieType:
            media = new Movie(id);
            break;
        case MediaType::BookType:
            media = new Book(id);
            break;
        case MediaType::Tv_showType:
            media = new Tv_show(id);
            break;
    }

    media->GetForm(this);
}

/**
 * Sets media properties with form data and submit it to the database
 *
 * @brief MediaWidget::SaveForm
 */
void MediaWidget::SaveForm()
{
    media->UpdateFromForm(this);
    media->Save();
    emit Saved();
}
