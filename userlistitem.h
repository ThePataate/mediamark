#pragma once

#include <QDate>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QSpinBox>
#include <QDateEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QComboBox>

#include <enums.h>
#include <mmdatabase.h>
#include <media.h>
#include <userlistitemformwidget.h>

class UserListItem
{
public:
    UserListItem(int media_id);
    ~UserListItem();
    void Save();
    void GetForm(QWidget *w);
    void UpdateFromForm();
    static bool AlreadyInList(int media_id);

    const QString &getStatus() const;
    void setStatus(UserListStatus newStatus);
    int getProgression() const;
    void setProgression(int newProgression);
protected:
    /**
     * @brief media_id
     */
    int media_id;

    /**
     * @brief status
     */
    QString status;

    /**
     * @brief progression
     */
    int progression;

    /**
     * @brief begin_date
     */
    QDate begin_date;

    /**
     * @brief end_date
     */
    QDate end_date;

    /**
     * @brief exists
     */
    bool exists = false;

    /**
     * @brief form
     */
    UserListItemFormWidget *form = nullptr;
};
