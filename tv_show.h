#pragma once

#include <QFormLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QDateEdit>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QDateTime>

#include "media.h"

class Tv_show : public Media
{
public:
    Tv_show(int id);
protected:
    QString director;
    QDate end_date;
    int season_number;

    // Media interface
public:
    void Save() override;
    void GetForm(QWidget *w) override;
    void UpdateFromForm(QWidget *w) override;
};
