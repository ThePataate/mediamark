#pragma once
/**
 * @brief The MediaType enum
 */
enum MediaType {MovieType = 0, Tv_showType = 1, BookType = 2, Tv_show_episodeType = 3};

enum UserListStatus {Started = 0, Finished = 1, Planned = 2, Dropped = 3};
