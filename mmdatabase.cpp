#include "mmdatabase.h"

//Static properties
bool MMDatabase::connected = false;
int MMDatabase::lastInsertId = 0;
const QString MMDatabase::Driver = "QSQLITE";
const QString MMDatabase::DatabaseName = "MediaMark.db";

/**
 * Connects the software to the sqlite database
 * @brief MMDatabase::ConnectToDatabase
 * @return connection successful
 */
bool MMDatabase::ConnectToDatabase()
{
    //SQLite is not available
    if(!QSqlDatabase::isDriverAvailable(MMDatabase::Driver))
    {
        MMDatabase::connected = false;
    }else{
        //Sets the database connection
        QSqlDatabase db = QSqlDatabase::addDatabase(Driver);
        db.setDatabaseName(MMDatabase::DatabaseName);

        //Database connection failed
        if(!db.open())
        {
            qWarning() << db.lastError();
            MMDatabase::connected = false;
        }else{
            MMDatabase::connected = true;
        }
    }
    return MMDatabase::connected;
}

/**
 * Setups the database if needed
 * @brief MMDatabase::SetupDatabase
 * @return setup successful
 */
bool MMDatabase::SetupDatabase()
{
    if(!MMDatabase::connected)
    {
        ConnectToDatabase();
    }

    QSqlQuery query;

    //Checks if media table exists
    if(query.exec("SELECT name FROM sqlite_master WHERE type='table' AND name='media';"))
    {
        if(query.next())
        {
            //qInfo() << "Media table already exists";
        }else{
            //qInfo () << "Media table doesnt exist";
            InsertAllMandatoryTables();
        }

    }else{
        qInfo() << "Table query failed";
        return false;
    }

    return true;
}

/**
 * Creates all database tables from the sql file
 *
 * @brief MMDatabase::InsertAllMandatoryTables
 */
void MMDatabase::InsertAllMandatoryTables()
{
    QFile sqlFile("db.sql");
    sqlFile.open(QFile::ReadOnly);
    QTextStream ts(&sqlFile);
    QSqlQuery query;
    query.exec(ts.readAll());
}

QSqlQuery MMDatabase::Query(QString sql, const QVector<QString> &values)
{
    QSqlQuery query;
    query.prepare(sql);

    for(int i(0); i < values.size(); i++)
    {
        query.addBindValue(values[i]);
    }

    query.exec();

    lastInsertId = query.lastInsertId().toInt();

    return query;
}

int MMDatabase::LastId()
{
    return lastInsertId ;
}



