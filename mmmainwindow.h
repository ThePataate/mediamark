#pragma once

#include <QMainWindow>
#include <QMenuBar>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QButtonGroup>
#include <QInputDialog>
#include <QStackedLayout>
#include <QTabWidget>
#include <QToolBar>
#include <QMessageBox>

#include <mediawidget.h>
#include <userlistwidget.h>
#include <mediatablewidget.h>

/**
 * @brief The MMMainWindow class
 *
 * Custom MainWindow class
 */
class MMMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MMMainWindow(QWidget *parent = nullptr);
private:
    /**
     * @brief baseSize
     *
     * Base size for the software window
     */
    QSize baseSize;

    /**
     * @brief menuBar
     */
    QMenuBar *menuBar;

    /**
     * @brief mainWidget
     */
    QWidget *mainWidget;

    /**
     * @brief mainWidgetHLayout
     */
    QVBoxLayout *mainWidgetVLayout;

    /**
     * @brief holder
     */
    QWidget *holder;

    /**
     * @brief tabWidget
     */
    QTabWidget *tabWidget;

    /**
     * @brief stackLayout
     */
    QStackedLayout *stackLayout;

    /**
     * @brief mediaWidget
     */
    QWidget *mediaWidget;

    /**
     * @brief mediaWidgetHolder
     */
    QWidget *mediaWidgetHolder;

    /**
     * @brief searchbar
     */
    QLineEdit *searchbar;

    void InitProperties();
    void InitTabs();
    void GoToFormPage(MediaType type, int id = 0);
    void GoToListItemPage(int id);
public slots:
    void ChangePage(int index);
    void UpdateCurrentList();
    void GoToList();
    void GoToNewMedia();
    void GoToUpdatePage(int);
    void AddToList(int id);
    void Search();
};
