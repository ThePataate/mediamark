#include "mediasearchquerymodel.h"

/**
 * Constructor
 *
 * @brief MediaSearchQueryModel::MediaSearchQueryModel
 * @param filter
 * @param parent
 */
MediaSearchQueryModel::MediaSearchQueryModel(QString filter, QObject *parent) : QSqlQueryModel(parent)
{
    SetCustomQuery(filter);

    setHeaderData(0, Qt::Horizontal, "Id");
    setHeaderData(1, Qt::Horizontal, "Name");
    setHeaderData(2, Qt::Horizontal, "Type");
    setHeaderData(3, Qt::Horizontal, "Update");
    setHeaderData(4, Qt::Horizontal, "Add To List");
}

/**
 * Sets this model's query
 *
 * @brief MediaSearchQueryModel::SetCustomQuery
 * @param filter
 */
void MediaSearchQueryModel::SetCustomQuery(QString filter)
{
    QString likefilter = "%" + filter + "%";

    setQuery(
        MMDatabase::Query(
            "SELECT id, name, movie.media_id as 'Movie', tv_show.media_id as 'TV Show', book.media_id as 'Book' "
            "FROM media "
            "LEFT JOIN movie ON movie.media_id = media.id "
            "LEFT JOIN tv_show ON tv_show.media_id = media.id "
            "LEFT JOIN book ON book.media_id = media.id "
            "WHERE name LIKE ?"
            ,
            {
                likefilter
            }
        )
    );
}

/**
 * @brief MediaSearchQueryModel::data
 * @param index
 * @param role
 * @return QVariant
 */
QVariant MediaSearchQueryModel::data(const QModelIndex &index, int role) const
{
    QVariant value = QSqlQueryModel::data(index, role);
    if(role == Qt::DisplayRole && value.isValid())
    {
        if(index.column() == 2)
        {
            if(value != "")
            {
                return "Movie";
            }
            if(QSqlQueryModel::data(this->index(index.row(), 3), role) != "")
            {
                return "Tv show";
            }
            else{
                return "Book";
            }
        }
        if(index.column() == 3 || index.column() == 4)
        {
            return "=>";
        }
    }

    return value;
}
