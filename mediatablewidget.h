#pragma once

#include <QWidget>
#include <QHBoxLayout>
#include <QTableView>
#include <QHeaderView>

#include <mediaquerymodel.h>
#include <mediasearchquerymodel.h>
#include <userlistquerymodel.h>

class MediaTableWidget : public QWidget
{
    Q_OBJECT
public:
    enum ViewType {Movie, Tv_show, Book, MediaSearch, UserList};

    explicit MediaTableWidget(ViewType type, QString filter = "", QWidget *parent = nullptr);
    ~MediaTableWidget();

    void SetFilter(const QString &filter);
protected:
    /**
     * @brief model
     */
    QSqlQueryModel *model;

    /**
     * @brief tableView
     */
    QTableView *tableView;

    /**
     * @brief viewType
     */
    ViewType viewType;

    /**
     * @brief filter
     */
    QString filter;
public slots:
    void GoToUpdatePage(const QModelIndex &index);
    void Refresh();
signals:
    /**
     * Signal emited when update cell has been double clicked
     * @brief UpdateButtonClicked
     * @param id
     */
    void UpdateButtonClicked(int id);

    /**
     * Signal emited when add to list cell has been double clicked
     * @brief AddToListButtonClicked
     * @param id
     */
    void AddToListButtonClicked(int id);
};
